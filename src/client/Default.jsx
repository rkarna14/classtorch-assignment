// Default layout template
import React from 'react';

const defaultTemplate = () => (
  <html lang="en">
    <head>
      <meta charSet="utf-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
      <title>Classtorch</title>
      <link href="static/bundle.css" rel="stylesheet" />
    </head>
    <body>
      <div id="app-root" />
      <script src="static/bundle.js" />
    </body>
  </html>
);

export default defaultTemplate;
