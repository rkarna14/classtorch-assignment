import { combineEpics } from 'redux-observable';
import { resultsEpics } from '../modules/results/reducer';

const rootEpic = combineEpics(resultsEpics);

export default rootEpic;
