import { combineReducers } from 'redux';
import resultsReducer from '../modules/results/reducer';
import admissionReducer from '../modules/admission/reducer';

const appReducers = combineReducers({
  results: resultsReducer,
  admission: admissionReducer,
});

export default (state, action) => appReducers(state, action);

