import { createStore, applyMiddleware, compose } from 'redux';
import { createEpicMiddleware } from 'redux-observable';

import rootReducer from './reducers';
import rootEpic from './epics';

const loadStateFromLocalStorage = () => {
  try {
    const serializedState = localStorage.getItem('redux-store');
    if (serializedState === null) {
      return undefined;
    }
    return JSON.parse(serializedState);
  } catch (err) {
    console.log('No store found in local storage');
    return undefined;
  }
};

const saveStateToLocalStorage = (state) => {
  try {
    const serializedState = JSON.stringify(state);
    window.localStorage.setItem('redux-store', serializedState);
  } catch (err) {
    console.log('Could not save application state to local storage');
  }
};

const epicMiddleware = createEpicMiddleware();
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(
  rootReducer,
  loadStateFromLocalStorage(),
  composeEnhancers(applyMiddleware(epicMiddleware)),
);

epicMiddleware.run(rootEpic);

store.subscribe(() => {
  saveStateToLocalStorage(store.getState());
}, 1000);

export default store;
