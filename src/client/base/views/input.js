import React, { Component } from 'react';
import PropTypes from 'prop-types';

const validate = {
  text: (validation, value) => {
    let isValid = true;
    if (validation.regex) {
      isValid = isValid && validation.regex.test(value);
    }
    if (validation.maxLength) {
      isValid = isValid && value && value.length < validation.maxLength;
    }
    return isValid;
  },
  number: (validation, value) => {
    let isValid = true;
    if (isNaN(value)) {
      isValid = false;
    }
    if (isValid && typeof validation.maxValue !== 'undefined') {
      isValid = parseFloat(value) <= validation.maxValue;
    }
    if (isValid && typeof validation.minValue !== 'undefined') {
      isValid = parseFloat(value) >= validation.minValue;
    }
    return isValid;
  },
};

class InputField extends Component {
  constructor(props) {
    super(props);
    this.state = { isDirty: false };
  }

  validator = () => {
    this.setState({ isDirty: true });
    const { type, validation, value } = this.props;
    const isValid = validate[type](validation, value);
    this.props.validationHandler(this.props.accessor, isValid);
  };

  render() {
    const {
      type, accessor, label, value, changeHandler, isValid,
    } = this.props;
    const showInvalidBorder = this.state.isDirty && !isValid;
    return (
      <label htmlFor={accessor} className="input-field">
        <div className="label">{label}</div>
        <input
          className={`${showInvalidBorder ? 'invalid' : ''}`}
          name={accessor}
          type={type}
          value={value}
          onChange={changeHandler}
          onBlur={this.validator}
        />
      </label>
    );
  }
}

InputField.propTypes = {
  type: PropTypes.string.isRequired,
  accessor: PropTypes.oneOfType([PropTypes.string, PropTypes.array]).isRequired,
  label: PropTypes.string.isRequired,
  changeHandler: PropTypes.func.isRequired,
  validationHandler: PropTypes.func.isRequired,
  validation: PropTypes.objectOf(PropTypes.any),
  isValid: PropTypes.bool,
  value: PropTypes.any,
};

export default InputField;
