import React from 'react';

const getValue = (data, accessor) => {
  let fieldValue;
  if (Array.isArray(accessor)) {
    fieldValue = accessor.reduce(
      (prevAccessedValue, currentAccessorKey) => prevAccessedValue[currentAccessorKey] || {},
      data,
    );
  } else {
    fieldValue = data[accessor];
  }
  return fieldValue;
};

const Table = ({
  headers, subHeaders, dataAccessor, data,
}) => (
  <table>
    <thead>
      <tr>
        {headers.map(header => (
          <th key={header.title} className="table-header" colSpan={header.colSpan || 1}>
            {header.title}
          </th>
          ))}
      </tr>
      <tr>
        {subHeaders.map((subHeader, index) => (
          <th key={index} className="table-subheader">
            {subHeader}
          </th>
          ))}
      </tr>
    </thead>
    <tbody>
      {data.map((rowdata, index) => (
        <tr key={index} className={`${rowdata.className || ''} table-data-row`}>
          {dataAccessor.map((cellDataAccessor, dataIndex) => (
            <td key={dataIndex}>{getValue(rowdata, cellDataAccessor)}</td>
            ))}
        </tr>
        ))}
    </tbody>
  </table>
);

export default Table;
