import React from 'react';
import { render } from 'react-dom';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { Provider } from 'react-redux';
import store from './base/store';
import AuthContextHandler from './modules/authcontext/authContextHandler';

require('./static/scss/index.scss');

const Index = () => (
  <BrowserRouter>
    {
      <Switch>
        <Route
          path="/"
          component={() => <AuthContextHandler />}
        />
      </Switch>
    }
  </BrowserRouter>
);

render(
  <Provider store={store}>
    <Index />
  </Provider >,
  document.getElementById('app-root'),
);
