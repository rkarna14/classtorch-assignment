import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';

import InputField from '../../base/views/input';
import { onFormDataChange, handleValidation } from './reducer';
import formSchema from './schema';

class AdmissionForm extends Component {
  changeHandler = event => {
    this.props.onFormDataChange(event.target.getAttribute('name'), event.target.value);
  };

  validationHandler = (key, isValid) => {
    this.props.handleValidation(key, isValid);
  };

  isFormInvalid = () => {
    const { validationErrors, formData } = this.props;
    const isAnyFieldMissing = Object.keys(formData).length !== formSchema.length;
    const isAnyFieldInvalid = Object.keys(validationErrors)
      .map(key => validationErrors[key])
      .some(validationStatus => validationStatus === false);
    return isAnyFieldMissing || isAnyFieldInvalid;
  };

  render() {
    return (
      <div>
        {formSchema.map(formField => (
          <InputField
            key={formField.accessor}
            value={this.props.formData[formField.accessor] || ''}
            isValid={this.props.validationErrors[formField.accessor]}
            validationHandler={this.validationHandler}
            {...formField}
            changeHandler={this.changeHandler}
          />
        ))}
        {
          <button
            className="button submit-button"
            onClick={() => {
              /* alert('Form Submitted'); */
            }}
            disabled={this.isFormInvalid()}
            value="Submit"
          >
            Submit
          </button>
        }
      </div>
    );
  }
}

const mapStateToProps = store => ({
  formData: store.admission.formData,
  validationErrors: store.admission.validationErrors,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      onFormDataChange,
      handleValidation,
    },
    dispatch,
  );

AdmissionForm.propTypes = {
  formData: PropTypes.objectOf(PropTypes.any).isRequired,
  validationErrors: PropTypes.objectOf(PropTypes.any).isRequired,
  onFormDataChange: PropTypes.func.isRequired,
  handleValidation: PropTypes.func.isRequired,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(AdmissionForm);
