const ON_FORM_DATA_CHANGE = 'ON_FORM_DATA_CHANGE';
const HANDLE_VALIDATION_ERROR_CHANGE = 'HANDLE_VALIDATION_ERROR_CHANGE';

export const onFormDataChange = (key, value) => ({
  type: ON_FORM_DATA_CHANGE,
  payload: {
    key,
    value,
  },
});

export const handleValidation = (key, value) => ({
  type: HANDLE_VALIDATION_ERROR_CHANGE,
  payload: {
    key,
    value,
  },
});

const init = {
  formData: {},
  validationErrors: {},
};

export default (state = init, action) => {
  switch (action.type) {
    case ON_FORM_DATA_CHANGE: {
      const { key, value } = action.payload;
      return { ...state, formData: { ...state.formData, [key]: value } };
    }

    case HANDLE_VALIDATION_ERROR_CHANGE: {
      const { key, value } = action.payload;
      return { ...state, validationErrors: { ...state.validationErrors, [key]: value } };
    }

    default: {
      return state;
    }
  }
};
