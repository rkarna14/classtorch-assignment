export default [
  {
    label: 'Name',
    accessor: 'name',
    type: 'text',
    validation: {
      required: true,
      maxLength: 20,
      regex: /^[A-Za-z]+$/,
    },
  },
  {
    label: 'Last Name',
    accessor: 'lastName',
    type: 'text',
    validation: {
      required: true,
      maxLength: 20,
      regex: /^[A-Za-z]+$/,
    },
  },
  {
    label: 'Class',
    accessor: 'class',
    type: 'text',
    validation: {
      required: true,
      regex: /^[A-Za-z0-9]+$/,
    },
  },
  {
    label: 'Year of Passing',
    accessor: 'yearOfPassing',
    type: 'number',
    validation: {
      required: true,
      maxValue: 2017,
      minValue: 0,
    },
  },
  {
    label: 'Percentage of marks',
    accessor: 'percentageOfMarks',
    type: 'number',
    validation: {
      required: true,
      maxValue: 100,
      minValue: 0,
    },
  },
];
