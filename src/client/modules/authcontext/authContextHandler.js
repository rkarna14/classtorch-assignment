import React from 'react';
import AuthenticatedSection from './authenticated';

const AuthenticationContextHandler = () => (
  <div className="application-container">
    {
      <AuthenticatedSection />
    }
  </div>
);

export default AuthenticationContextHandler;
