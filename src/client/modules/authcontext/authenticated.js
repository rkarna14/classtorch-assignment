import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Results from '../results';
import Admission from '../admission';
import Navigation from '../navigation';

const Authenticated = () => (
  <div>
    {<Navigation authorizedNavItems={[]} />}
    <div className="main-content squeezed">
      <Switch>
        <Route path="/results" component={() => <Results />} />
        <Route path="/admission" component={() => <Admission />} />
      </Switch>
    </div>
  </div>
);

export default Authenticated;
