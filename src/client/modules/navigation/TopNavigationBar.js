import React from 'react';
import { Link } from 'react-router-dom';

const TopNavigationBar = () => (
  <div className="top-navigation-bar">
    <Link to="/results">Results</Link>
    <Link to="/admission">Admission</Link>
  </div>
);

export default TopNavigationBar;
