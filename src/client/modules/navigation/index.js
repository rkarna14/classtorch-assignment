import React from 'react';
import TopBar from './TopNavigationBar';

const Navigation = () => (
  <React.Fragment>
    <TopBar />
  </React.Fragment>
);

export default Navigation;
