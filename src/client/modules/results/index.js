import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Table from '../../base/views/table';
import { fetchResults } from './reducer';

const FULL_MARKS = 50;
const PASS_MARKS = 20;
const PASS = 'PASS';
const FAIL = 'FAIL';
const TOPPER = 'TOPPER';

const getSortedData = (data, sortKey) =>
  data.sort((info1, info2) => {
    if (info1[sortKey] < info2[sortKey]) {
      return -1;
    }
    if (info1[sortKey] > info2[sortKey]) {
      return 1;
    }
    return 0;
  });

const getFormattedDataForTable = data => {
  if (!data || !data.length) {
    return [];
  }
  const enrichedData = data.map(info => {
    const totalMarks = Object.keys(info.marks).reduce(
      (accumulator, subName) => info.marks[subName] + accumulator,
      0,
    );
    const hasPassedAllSubs = Object.keys(info.marks).reduce(
      (failedStatusAccumulator, subName) =>
        info.marks[subName] >= PASS_MARKS && failedStatusAccumulator,
      1,
    );
    let status = FAIL;
    let className = 'fail';
    if (hasPassedAllSubs) {
      status = PASS;
      className = 'pass';
    }
    return {
      ...info, totalMarks, status, className,
    };
  });
  const dataSortedByTotalMarks = getSortedData(enrichedData, 'totalMarks');
  dataSortedByTotalMarks[dataSortedByTotalMarks.length - 1].status = TOPPER;
  dataSortedByTotalMarks[dataSortedByTotalMarks.length - 1].className = 'topper';
  return getSortedData(dataSortedByTotalMarks, 'name');
};

class Results extends Component {
  componentDidMount() {
    this.props.fetchResults();
  }
  render() {
    const { result } = this.props;
    const formattedData = getFormattedDataForTable(result || []);
    const headers = [
      { title: 'Student Name' },
      { title: 'Roll Number' },
      { title: 'Marks', colSpan: 3 },
      { title: 'Total Marks' },
      { title: 'Status' },
    ];
    const subHeaders = ['', '', 'Maths', 'English', 'Science', '', ''];
    const dataAccessor = [
      'name',
      'rollNumber',
      ['marks', 'Maths'],
      ['marks', 'English'],
      ['marks', 'Science'],
      'totalMarks',
      'status',
    ];
    return (
      <Table
        headers={headers}
        subHeaders={subHeaders}
        dataAccessor={dataAccessor}
        data={formattedData}
      />
    );
  }
}

const mapStateToProps = (store) => ({
  result: store.results.result,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      fetchResults,
    },
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(Results);
