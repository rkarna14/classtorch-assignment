import { ajax } from 'rxjs/ajax';
import { Observable } from 'rxjs/Observable';
import { combineEpics } from 'redux-observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/switchMap';

const FETCH_RESULTS = 'FETCH_RESULTS';
const FETCH_RESULTS_SUCCESS = 'FETCH_RESULTS_SUCCESS';

const init = {
  result: [],
};

export const fetchResults = () => ({
  type: FETCH_RESULTS,
});

export default ((state = init, action) => {
  switch (action.type) {
    case FETCH_RESULTS_SUCCESS: {
      const { success, data } = action.payload;
      if (success) {
        return { ...state, result: data };
      }
      return state;
    }
    default: {
      return state;
    }
  }
});

const fetchResultsEpic = action$ =>
  action$
    .ofType(FETCH_RESULTS)
    .switchMap(() => ajax.get('/results-data').map(results => results.response))
    .map(payload => ({ type: FETCH_RESULTS_SUCCESS, payload }));

export const resultsEpics = combineEpics(fetchResultsEpic);
