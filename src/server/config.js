export default {
  serverOptions: {
    PORT: process.env.port || 3001,
    HOST: process.env.host || 'localhost',
  },
};

