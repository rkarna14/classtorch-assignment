// import BabelPolyFill from 'babel-polyfill'; /* Enable for debug */

import bootstrapServer from './serverManager';
import routes from './routes';

process.on('unhandledRejection', (err) => {
  console.log('Exiting server instance due to unhandled rejection');
  console.log(err);
  process.exit(1);
});

const initServer = async () => {
  const serverManager = await bootstrapServer(routes);
  serverManager.server.start();
  console.log('Server started at localhost:3001');
};

initServer();

