const routes = [
  {
    method: 'GET',
    path: '/',
    options: { auth: false },
    handler: {
      view: 'Default',
    },
  },
  {
    method: 'GET',
    path: '/favicon.ico',
    options: { auth: false },
    handler(request, h) {
      return h.file(`${process.env.NODE_PATH}/src/client/static/img/favicon.ico`);
    },
  },
  {
    method: 'GET',
    path: '/static/{param*}',
    options: { auth: false },
    handler: {
      directory: {
        path: `${process.env.NODE_PATH}/src/client/static`,
        index: ['index.html'],
      },
    },
  },
  {
    method: 'GET',
    path: '/{param*}',
    options: { auth: false },
    handler: {
      view: 'Default',
    },
  },
  {
    method: 'GET',
    path: '/results-data',
    config: {
      handler: (request, h) => h.response({
        success: true,
        data: [
          {
            name: 'rajiv',
            marks: { Maths: 18, English: 21, Science: 45 },
            rollNumber: 'KV2017-5A2',
          },
          {
            name: 'abhishek',
            marks: { Maths: 43, English: 30, Science: 37 },
            rollNumber: 'KV2017-5A1',
          },
          {
            name: 'zoya',
            marks: { Maths: 42, English: 31, Science: 50 },
            rollNumber: 'KV2017-5A3',
          },
        ],
      }),
    },
  },
];

export default routes;
