import Hapi from 'hapi';
import Inert from 'inert';
import Vision from 'vision';
import hapiReactViews from 'hapi-react-views';

import config from './config';

class ServerManager {
  constructor() {
    const { HOST, PORT } = config.serverOptions;
    const options = {
      host: HOST,
      port: PORT,
    };
    this._server = new Hapi.Server(options);
  }

  get server() {
    return this._server;
  }

  async registerPlugin(pluginConfig) {
    try {
      await this._server.register(pluginConfig);
    } catch (error) {
      console.error(`Error while registering plugin: ${error.message}`);
    }
  }

  registerRoutes(routes) {
    this._server.route(routes);
  }

  configureViewEngine() {
    const viewConfig = {
      engines: {
        jsx: hapiReactViews,
      },
      relativeTo: process.env.NODE_PATH,
      path: 'src/client',
    };
    this._server.views(viewConfig);
  }
}

const bootstrapServer = async (routes) => {
  const serverManager = new ServerManager(routes);
  await serverManager.registerPlugin([Inert, Vision]);
  serverManager.configureViewEngine();
  serverManager.registerRoutes(routes);
  return serverManager;
};

export default bootstrapServer;
